package uz.personal;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Integer.valueOf;

public class CSVParser {

    private static final com.opencsv.CSVParser PARSER = new CSVParserBuilder().withSeparator(';').build();


    public static List<Pair<String, Integer>> convertToList(String file) {

        try {

            FileReader filereader = new FileReader(file);

            CSVReader csvReader = new CSVReaderBuilder(filereader)
                    .withCSVParser(PARSER)
                    .build();
            List<String[]> lines = csvReader.readAll();

            List<Pair<String, Integer>> pairs = new ArrayList<>();
            lines.forEach(line -> pairs.add(new ImmutablePair<>(line[0], valueOf(line[1]))));
            return pairs;
        } catch (IOException | CsvException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
