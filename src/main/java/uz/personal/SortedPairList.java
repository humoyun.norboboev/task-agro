package uz.personal;

import lombok.With;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Comparator;
import java.util.List;

import static java.util.Comparator.comparingInt;

@With
public record SortedPairList(List<Pair<String, Integer>> pairs, Comparator<Pair<String, Integer>> comparator) {

    /**
     * By default, pairs are sorted by number
     **/

    public SortedPairList(List<Pair<String, Integer>> pairs) {
        this(pairs, comparingInt(Pair::getRight));
    }

    /**
     * Sorted by java's default sorting algorithm which is DualPivotQuickSort
     **/

    public void sort() {
        pairs.sort(comparator);
    }

    /**
     * Sorted by QuickSort
     **/

    public void quickSort() {
        quickSort(0, pairs.size() - 1);
    }


    private void quickSort(int begin, int end) {
        if (begin < end) {
            int partitionIndex = partition(begin, end);

            quickSort(begin, partitionIndex - 1);
            quickSort(partitionIndex + 1, end);
        }
    }

    private int partition(int begin, int end) {
        Pair<String, Integer> pivot = pairs.get(end);
        int i = (begin - 1);
        for (int j = begin; j < end; j++) {
            if (comparator.compare(pairs.get(j), pivot) < 0) {
                i++;
                Pair<String, Integer> swapTemp = pairs.get(i);
                pairs.set(i, pairs.get(j));
                pairs.set(j, swapTemp);
            }
        }
        Pair<String, Integer> swapTemp = pairs.get(i + 1);
        pairs.set(i + 1, pairs.get(end));
        pairs.set(end, swapTemp);

        return i + 1;
    }
}
