package uz.personal;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import static java.util.Comparator.comparing;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static uz.personal.CSVParser.convertToList;

public class SortedPairListTest {

    @Test
    public void shouldSortPairsListByNumber() {
        var pairs = convertToList("task.csv");

        SortedPairList sortedPairList = new SortedPairList(pairs);
//  by default pairs are sorted by Number
        sortedPairList.quickSort();

        var firstElement = sortedPairList.pairs().get(0).getRight();
        var lastElement = sortedPairList.pairs().get(pairs.size() - 1).getRight();

        assertEquals(firstElement, 1);
        assertEquals(lastElement, 333);
    }

    @Test
    public void shouldSortPairsListByString() {
        var pairs = convertToList("task.csv");

        SortedPairList sortedPairList = new SortedPairList(pairs, comparing(Pair::getLeft));

        sortedPairList.quickSort();
        var firstElement = sortedPairList.pairs().get(0).getLeft();
        var lastElement = sortedPairList.pairs().get(pairs.size() - 1).getLeft();

        assertEquals(firstElement, "aaa");
        assertEquals(lastElement, "zzz");
    }
}
